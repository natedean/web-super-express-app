# web-super-express-app #

### What is this repository for? ###

Testing Azure App Service

### Environments ###

* [dev (develop branch)](https://web-super-express-app-development.azurewebsites.net)
* [test (release branch)](https://web-super-express-app-test.azurewebsites.net)
* [prod (master branch)](https://web-super-express-app.azurewebsites.net)

